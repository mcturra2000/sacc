# sacc: saccomys gopher client
# See LICENSE file for copyright and license details.
.POSIX:


# Install paths
PREFIX = $(HOME)/.local
MANDIR = $(PREFIX)/share/man/man1

# Default build flags
CFLAGS = -Os
CFLAGS = -ggdb -O0
CXXFLAGS = $(CFLAGS)
CXXFLAGS += -std=c++20 -Wno-write-strings
#LDFLAGS = -s -ggdb
LDFLAGS =  -ggdb


LIBS=-lcurses -lcurl

OSCFLAGS = -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=700 -D_BSD_SOURCE -D_GNU_SOURCE
#OSLDFLAGS = 

# Define NEED_ASPRINTF and/or NEED_STRCASESTR in your OS compilation flags
# if your system does not provide asprintf() or strcasestr(), respectively.
#OSCFLAGS = -DNEED_ASPRINTF -DNEED_STRCASESTR
BIN = sacc
MAN = $(BIN).1
OBJ = sacc.o ui_ti.o io_clr.o
CC = gcc

GETVER = $$(git rev-parse --is-inside-work-tree >/dev/null 2>&1 \
	&& git describe --tags \
	|| echo $(DEFVERSION))

all: $(BIN)


$(BIN): $(OBJ)
	$(CXX) $(SACCLDFLAGS) -o $@ $(OBJ) $(IOLIBS) $(LIBS)

$(OBJ): common.h
sacc.o: config.h 
ui_ti.o: config.h
io_$(IO).o: io.h

clean:
	rm -f $(BIN) $(OBJ)

install: $(BIN)
	mkdir -p $(DESTDIR)$(PREFIX)/bin/
	cp -f $(BIN) $(DESTDIR)$(PREFIX)/bin/
	chmod 555 $(DESTDIR)$(PREFIX)/bin/$(BIN)
	mkdir -p $(DESTDIR)$(MANDIR)
	sed -e "s/%VERSION%/$(GETVER)/" $(MAN) > $(DESTDIR)$(MANDIR)/$(MAN)

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/$(BIN) $(DESTDIR)$(MANDIR)/$(MAN)

# Stock FLAGS
SACCCFLAGS = $(OSCFLAGS) \
             $(IOCFLAGS) \
             $(CFLAGS) \

SACCLDFLAGS = $(OSLDFLAGS) \
	      $(LDFLAGS) \

.git/refs/heads/:

.c.o: 
	$(CC) $(SACCCFLAGS) -c $<

sacc.o : sacc.cc
	g++ $(CXXFLAGS) -c $< 
