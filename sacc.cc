#include <curl/curl.h>
#include <string>
#include <iostream>
#include <vector>
//#include <format>
#include <iomanip>
#include <fstream>

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <locale.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <wchar.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>


#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

using namespace std;

/*
#define MAX 80 
#define PORT 8080 
#define SA struct sockaddr 
void func(int sockfd) 
{ 
char buff[MAX]; 
char resp[MAX+20]; 
int n; 
for (;;) { 
bzero(buff, sizeof(buff)); 
printf("Enter the string : "); 
n = 0; 
while ((buff[n++] = getchar()) != '\n') 
; 
//puts("");
write(sockfd, buff, sizeof(buff)); 
bzero(resp, sizeof(resp)); 
read(sockfd, resp, sizeof(resp)); 
printf("From Server : %s", resp); 
if ((strncmp(buff, "exit", 4)) == 0) { 
printf("Client Exit...\n"); 
break; 
} 
} 
} 
*/

#include "common.h"
#include "io.h"

enum {
	TXT,
	DIR,
	CSO,
	ERR,
	MAC,
	DOS,
	UUE,
	IND,
	TLN,
	BIN,
	MIR,
	IBM,
	GIF,
	IMG,
	URL,
	INF,
	UNK,
	BRK,
};

#define NEED_CONF
#include "config.h"
#undef NEED_CONF

void (*diag)(char *, ...);

int interactive;

static char intbuf[256]; /* 256B ought to be enough for any URI */
static char *mainurl;
static Item *mainentry;
static int devnullfd;
static int parent = 1;

	static void
stddiag(char *fmt, ...)
{
	va_list arg;

	va_start(arg, fmt);
	vfprintf(stderr, fmt, arg);
	va_end(arg);
	fputc('\n', stderr);
}

	void
die(const char *fmt, ...)
{
	va_list arg;

	va_start(arg, fmt);
	vfprintf(stderr, fmt, arg);
	va_end(arg);
	fputc('\n', stderr);

	exit(1);
}

#ifdef NEED_ASPRINTF
	int
asprintf(char **s, const char *fmt, ...)
{
	va_list ap;
	int n;

	va_start(ap, fmt);
	n = vsnprintf(NULL, 0, fmt, ap);
	va_end(ap);

	if (n == INT_MAX || !(*s = malloc(++n)))
		return -1;

	va_start(ap, fmt);
	vsnprintf(*s, n, fmt, ap);
	va_end(ap);

	return n;
}
#endif /* NEED_ASPRINTF */

#ifdef NEED_STRCASESTR
	char *
strcasestr(const char *h, const char *n)
{
	size_t i;

	if (!n[0])
		return (char *)h;

	for (; *h; ++h) {
		for (i = 0; n[i] && tolower((unsigned char)n[i]) ==
				tolower((unsigned char)h[i]); ++i)
			;
		if (n[i] == '\0')
			return (char *)h;
	}

	return NULL;
}
#endif /* NEED_STRCASESTR */

/* FN mbsprint 
 * print `len' columns of characters. */
	size_t
mbsprint (const char *s, size_t len)
{
	wchar_t wc;
	size_t col = 0, i, slen;
	const char *p;
	int rl, pl, w;

	if (!len)
		return col;

	slen = strlen(s);
	for (i = 0; i < slen; i += rl) {
		rl = mbtowc(&wc, s + i, slen - i < 4 ? slen - i : 4);
		if (rl == -1) {
			/* reset state */
			mbtowc(NULL, NULL, 0);
			p = "\xef\xbf\xbd"; /* replacement character */
			pl = 3;
			rl = w = 1;
		} else {
			if ((w = wcwidth(wc)) == -1)
				continue;
			pl = rl;
			p = s + i;
		}
		if (col + w > len || (col + w == len && s[i + rl])) {
			fputs("\xe2\x80\xa6", stdout); /* ellipsis */
			col++;
			break;
		}
		fwrite(p, 1, pl, stdout);
		col += w;
	}
	return col;
}

	static void *
xreallocarray(void *m, size_t n, size_t s)
{
	void *nm;

	if (n == 0 || s == 0) {
		free(m);
		return NULL;
	}
	if (s && n > (size_t)-1/s)
		die("realloc: overflow");
	if (!(nm = realloc(m, n * s)))
		die("realloc: %s", strerror(errno));

	return nm;
}

	static void *
xmalloc(const size_t n)
{
	void *m = malloc(n);

	if (!m)
		die("malloc: %s", strerror(errno));

	return m;
}

	static void *
xcalloc(size_t n)
{
	char *m = (char *) calloc(1, n);

	if (!m)
		die("calloc: %s", strerror(errno));

	return m;
}

	static char *
xstrdup(const char *str)
{
	char *s;

	if (!(s = strdup(str)))
		die("strdup: %s", strerror(errno));

	return s;
}

	static void
usage(void)
{
	die("usage: sacc URL");
}

	static void
clearitem(Item *item)
{
	Dir *dir;
	Item *items;
	char *tag;
	size_t i;

	if (!item)
		return;

	if (dir =(Dir*) item->dat) {
		items = dir->items;
		for (i = 0; i < dir->nitems; ++i)
			clearitem(&items[i]);
		free(items);
		clear(&item->dat);
	}

	if (parent && (tag = item->tag) &&
			!strncmp(tag, tmpdir, strlen(tmpdir)))
		unlink(tag);

	clear(&item->tag);
	clear(&item->raw);
}

	const char *
typedisplay(char t)
{
	switch (t) {
		case '0':
			return typestr[TXT];
		case '1':
			return typestr[DIR];
		case '2':
			return typestr[CSO];
		case '3':
			return typestr[ERR];
		case '4':
			return typestr[MAC];
		case '5':
			return typestr[DOS];
		case '6':
			return typestr[UUE];
		case '7':
			return typestr[IND];
		case '8':
			return typestr[TLN];
		case '9':
			return typestr[BIN];
		case '+':
			return typestr[MIR];
		case 'T':
			return typestr[IBM];
		case 'g':
			return typestr[GIF];
		case 'I':
			return typestr[IMG];
		case 'h':
			return typestr[URL];
		case 'i':
			return typestr[INF];
		default:
			/* "Characters '0' through 'Z' are reserved." (ASCII) */
			if (t >= '0' && t <= 'Z')
				return typestr[BRK];
			else
				return typestr[UNK];
	}
}

	int
itemuri(Item *item, char *buf, size_t bsz)
{
	int n;

	switch (item->type) {
		case '8':
			n = snprintf(buf, bsz, "telnet://%s@%s:%s",
					item->selector, item->host, item->port);
			break;
		case 'T':
			n = snprintf(buf, bsz, "tn3270://%s@%s:%s",
					item->selector, item->host, item->port);
			break;
		case 'h':
			n = snprintf(buf, bsz, "%s", item->selector +
					(strncmp(item->selector, "URL:", 4) ? 0 : 4));
			break;
		default:
			n = snprintf(buf, bsz, "gopher://%s", item->host);

			if (n < bsz-1 && strcmp(item->port, "70"))
				n += snprintf(buf+n, bsz-n, ":%s", item->port);
			if (n < bsz-1) {
				n += snprintf(buf+n, bsz-n, "/%c%s",
						item->type, item->selector);
			}
			if (n < bsz-1 && item->type == '7' && item->tag) {
				n += snprintf(buf+n, bsz-n, "%%09%s",
						item->tag + strlen(item->selector));
			}
			break;
	}

	return n;
}

	static void
printdir(Item *item)
{
	Dir *dir;
	Item *items;
	size_t i, nitems;

	if (!item || !(dir = (Dir*)item->dat))
		return;

	items = dir->items;
	nitems = dir->nitems;

	for (i = 0; i < nitems; ++i) {
		printf("%s%s\n",
				typedisplay(items[i].type), items[i].username);
	}
}

	static void
displaytextitem(Item *item)
{
	struct sigaction sa;
	FILE *pagerin;
	int pid, wpid;

	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	sa.sa_handler = SIG_DFL;
	sigaction(SIGWINCH, &sa, NULL);

	uicleanup();

	switch (pid = fork()) {
		case -1:
			diag("Couldn't fork.");
			return;
		case 0:
			parent = 0;
			if (!(pagerin = popen("$PAGER", "w")))
				_exit(1);
			fputs(item->raw, pagerin);
			exit(pclose(pagerin));
		default:
			while ((wpid = wait(NULL)) >= 0 && wpid != pid)
				;
	}
	uisetup();

	sa.sa_handler = uisigwinch;
	sigaction(SIGWINCH, &sa, NULL);
	uisigwinch(SIGWINCH); /* force redraw */
}

	static char *
pickfield(char **raw, const char *sep)
{
	char c, *r, *f = *raw;

	for (r = *raw; (c = *r) && !strchr(sep, c); ++r) {
		if (c == '\n')
			goto skipsep;
	}

	*r++ = '\0';
skipsep:
	*raw = r;

	return f;
}

	static char *
invaliditem(char *raw)
{
	char c;
	int tabs;

	for (tabs = 0; (c = *raw) && c != '\n'; ++raw) {
		if (c == '\t')
			++tabs;
	}
	if (tabs < 3) {
		*raw++ = '\0';
		return raw;
	}

	return NULL;
}

	static void
downloaditem(Item *item)
{
	puts("TODO downloaditem");
}

	static int
download(Item *item, int dest)
{
	puts("TODO download");
	return 0;
}

	static void
molditem(Item *item, char **raw)
{
	char *next;

	if (!*raw)
		return;

	if ((next = invaliditem(*raw))) {
		item->username = *raw;
		*raw = next;
		return;
	}

	item->type = *raw[0]++;
	item->username = pickfield(raw, "\t");
	item->selector = pickfield(raw, "\t");
	item->host = pickfield(raw, "\t");
	item->port = pickfield(raw, "\t\r");
	while (*raw[0] != '\n')
		++*raw;
	*raw[0]++ = '\0';
}

/* FN molddiritem 
 * Splits the raw input for a gophermap into line-wise items
 */
	static Dir *
molddiritem(char *raw)
{
	Item *item, *items = NULL;
	char *nl, *p;
	Dir *dir;
	size_t i, n, nitems;

	for (nl = raw, nitems = 0; p = strchr(nl, '\n'); nl = p+1)
		++nitems;

	if (!nitems) {
		diag("Couldn't parse dir item");
		return NULL;
	}

	dir = (Dir*)xmalloc(sizeof(Dir));
	items = (Item*) xreallocarray(items, nitems, sizeof(Item));
	memset(items, 0, nitems * sizeof(Item));

	for (i = 0; i < nitems; ++i) {
		item = &items[i];
		molditem(item, &raw);
		if (item->type == '+') {
			for (n = i - 1; n < (size_t)-1; --n) {
				if (items[n].type != '+') {
					item->redtype = items[n].type;
					break;
				}
			}
		}
	}

	dir->items = items;
	dir->nitems = nitems;
	dir->printoff = dir->curline = 0;

	return dir;
}




	static int
sendselector(struct cnx *c, const char *selector)
{
	char *msg, *p;
	size_t ln;
	ssize_t n;

	ln = strlen(selector) + 3;
	msg = p = (char*) xmalloc(ln);
	snprintf(msg, ln--, "%s\r\n", selector);

	while ((n = iowrite(c, p, ln)) > 0) {
		ln -= n;
		p += n;
	};

	free(msg);
	if (n == -1)
		diag("Can't send message: %s", strerror(errno));

	return n;
}

	static int
connectto(const char *host, const char *port, struct cnx *c)
{
	sigset_t set, oset;
	static const struct addrinfo hints = {
		.ai_family = AF_UNSPEC,
		.ai_socktype = SOCK_STREAM,
		.ai_protocol = IPPROTO_TCP,
	};
	struct addrinfo *addrs, *ai;
	int r, err;

	sigemptyset(&set);
	sigaddset(&set, SIGWINCH);
	sigprocmask(SIG_BLOCK, &set, &oset);

	if (r = getaddrinfo(host, port, &hints, &addrs)) {
		diag("Can't resolve hostname \"%s\": %s",
				host, gai_strerror(r));
		goto err;
	}

	r = -1;
	for (ai = addrs; ai && r == -1; ai = ai->ai_next) {
		do {
			if ((c->sock = socket(ai->ai_family, ai->ai_socktype,
							ai->ai_protocol)) == -1) {
				err = errno;
				break;
			}

			if ((r = ioconnect(c, ai, host)) < 0) {
				err = errno;
				ioclose(c);
			}
		} while (r == CONN_RETRY);
	}

	freeaddrinfo(addrs);

	if (r == CONN_ERROR)
		ioconnerr(c, host, port, err);
err:
	sigprocmask(SIG_SETMASK, &oset, NULL);

	return r;
}



	static void
pipeuri(char *cmd, char *msg, char *uri)
{
	FILE *sel;

	if ((sel = popen(cmd, "w")) == NULL) {
		diag("URI not %s\n", msg);
		return;
	}

	fputs(uri, sel);
	pclose(sel);
	diag("%s \"%s\"", msg, uri);
}

	static void
execuri(char *cmd, char *msg, char *uri)
{
	switch (fork()) {
		case -1:
			diag("Couldn't fork.");
			return;
		case 0:
			parent = 0;
			dup2(devnullfd, 1);
			dup2(devnullfd, 2);
			if (execlp(cmd, cmd, uri, NULL) == -1)
				_exit(1);
		default:
			if (modalplumber) {
				while (waitpid(-1, NULL, 0) != -1)
					;
			}
	}

	diag("%s \"%s\"", msg, uri);
}

	static void
plumbitem(Item *item)
{
	char *file, *path, *tag;
	mode_t mode = S_IRUSR|S_IWUSR|S_IRGRP;
	int dest, plumbitem;

	if (file = strrchr(item->selector, '/'))
		++file;
	else
		file = item->selector;

	path = uiprompt("Download %s to (^D cancel, <empty> plumb): ",
			file);
	if (!path)
		return;

	if ((tag = item->tag) && access(tag, R_OK) == -1) {
		clear(&item->tag);
		tag = NULL;
	}

	plumbitem = path[0] ? 0 : 1;

	if (!path[0]) {
		clear(&path);
		if (!tag) {
			if (asprintf(&path, "%s/%s", tmpdir, file) == -1)
				die("Can't generate tmpdir path: %s/%s: %s",
						tmpdir, file, strerror(errno));
		}
	}

	if (path && (!tag || strcmp(tag, path))) {
		if ((dest = open(path, O_WRONLY|O_CREAT|O_EXCL, mode)) == -1) {
			diag("Can't open destination file %s: %s",
					path, strerror(errno));
			errno = 0;
			goto cleanup;
		}
		if (!download(item, dest) || tag)
			goto cleanup;
	}

	if (!tag)
		item->tag = path;

	if (plumbitem)
		execuri(plumber, "Plumbed", item->tag);

	return;
cleanup:
	free(path);
	return;
}

	void
yankitem(Item *item)
{
	itemuri(item, intbuf, sizeof(intbuf));
	pipeuri(yanker, "Yanked", intbuf);
}

	static int
dig(Item *entry, Item *item)
{
	char *plumburi = NULL;
	int t;

	if (item->raw) /* already in cache */
		return item->type;
	if (!item->entry)
		item->entry = entry ? entry : item;

	t = item->redtype ? item->redtype : item->type;
	switch (t) {
		case 'h': /* fallthrough */
			if (!strncmp(item->selector, "URL:", 4)) {
				execuri(plumber, "Plumbed", item->selector+4);
				return 0;
			}
		case '0':
			puts("TODO 0");
			//if (!fetchitem(item))
			return 0;
			break;
		case '1':
		case '7':
			puts("TODO 1/7");
			//if (!fetchitem(item) || !(item->dat = molddiritem(item->raw)))
			return 0;
			break;
		case '4':
		case '5':
		case '6':
		case '9':
			downloaditem(item);
			return 0;
		case '8':
			if (asprintf(&plumburi, "telnet://%s%s%s:%s",
						item->selector, item->selector ? "@" : "",
						item->host, item->port) == -1)
				return 0;
			execuri(plumber, "Plumbed", plumburi);
			free(plumburi);
			return 0;
		case 'T':
			if (asprintf(&plumburi, "tn3270://%s%s%s:%s",
						item->selector, item->selector ? "@" : "",
						item->host, item->port) == -1)
				return 0;
			execuri(plumburi, "Plumbed", plumburi);
			free(plumburi);
			return 0;
		default:
			if (t >= '0' && t <= 'Z') {
				diag("Type %c (%s) not supported", t, typedisplay(t));
				return 0;
			}
		case 'g':
		case 'I':
			plumbitem(item);
		case 'i':
			return 0;
	}

	return item->type;
}

	static char *
searchselector(Item *item)
{
	char *pexp, *exp, *tag, *selector = item->selector;
	size_t n = strlen(selector);

	if ((tag = item->tag) && !strncmp(tag, selector, n))
		pexp = tag + n+1;
	else
		pexp = "";

	if (!(exp = uiprompt("Enter search string (^D cancel) [%s]: ", pexp)))
		return NULL;

	if (exp[0] && strcmp(exp, pexp)) {
		n += strlen(exp) + 2;
		tag = (char*) xmalloc(n);
		snprintf(tag, n, "%s\t%s", selector, exp);
	}

	free(exp);
	return tag;
}

	static int
searchitem(Item *entry, Item *item)
{
	char *sel, *selector;

	if (!(sel = searchselector(item)))
		return 0;

	if (sel != item->tag)
		clearitem(item);
	if (!item->dat) {
		selector = item->selector;
		item->selector = item->tag = sel;
		dig(entry, item);
		item->selector = selector;
	}
	return (item->dat != NULL);
}

	static void
printout(Item *hole)
{
	char t = 0;

	if (!hole)
		return;

	switch (hole->redtype ? hole->redtype : (t = hole->type)) {
		case '0':
			if (dig(hole, hole))
				fputs(hole->raw, stdout);
			return;
		case '1':
		case '7':
			if (dig(hole, hole))
				printdir(hole);
			return;
		default:
			if (t >= '0' && t <= 'Z') {
				diag("Type %c (%s) not supported", t, typedisplay(t));
				return;
			}
		case '4':
		case '5':
		case '6':
		case '9':
		case 'g':
		case 'I':
			download(hole, 1);
		case '2':
		case '3':
		case '8':
		case 'T':
			return;
	}
}

	static void
delve(Item *hole)
{
	Item *entry = NULL;

	while (hole) {
		switch (hole->redtype ? hole->redtype : hole->type) {
			case 'h':
			case '0':
				if (dig(entry, hole))
					displaytextitem(hole);
				break;
			case '1':
			case '+':
				if (dig(entry, hole) && hole->dat)
					entry = hole;
				break;
			case '7':
				if (searchitem(entry, hole))
					entry = hole;
				break;
			case 0:
				diag("Couldn't get %s:%s/%c%s", hole->host,
						hole->port, hole->type, hole->selector);
				break;
			case '4':
			case '5':
			case '6': /* TODO decode? */
			case '8':
			case '9':
			case 'g':
			case 'I':
			case 'T':
			default:
				dig(entry, hole);
				break;
		}

		if (!entry)
			return;

		do {
			uidisplay(entry);
			hole = uiselectitem(entry);
		} while (hole == entry);
	}
}

	static Item *
moldentry(char *url)
{
	Item *entry;
	char *p, *host = url, *port = "70", *gopherpath = "1";
	int parsed, ipv6;

	host = ioparseurl(url);

	if (*host == '[') {
		ipv6 = 1;
		++host;
	} else {
		ipv6 = 0;
	}

	for (parsed = 0, p = host; !parsed && *p; ++p) {
		switch (*p) {
			case ']':
				if (ipv6) {
					*p = '\0';
					ipv6 = 0;
				}
				continue;
			case ':':
				if (!ipv6) {
					*p = '\0';
					port = p+1;
				}
				continue;
			case '/':
				*p = '\0';
				parsed = 1;
				continue;
		}
	}

	if (*host == '\0' || *port == '\0' || ipv6)
		die("Can't parse url");

	if (*p != '\0')
		gopherpath = p;

	entry = (item *) xcalloc(sizeof(Item));
	entry->type = gopherpath[0];
	entry->username = entry->selector = ++gopherpath;
	if (entry->type == '7') {
		if (p = strstr(gopherpath, "%09")) {
			memmove(p+1, p+3, strlen(p+3)+1);
			*p = '\t';
		}
		if (p || (p = strchr(gopherpath, '\t'))) {
			asprintf(&entry->tag, "%s", gopherpath);
			*p = '\0';
		}
	}
	entry->host = host;
	entry->port = port;
	entry->entry = entry;

	return entry;
}

	static void
cleanup(void)
{
	clearitem(mainentry);
	if (parent)
		rmdir(tmpdir);
	free(mainentry);
	free(mainurl);
	if (interactive)
		uicleanup();
}

	static void
sighandler(int signo)
{
	exit(128 + signo);
}

	static void
setup(void)
{
	struct sigaction sa;
	int fd;

	setlocale(LC_CTYPE, "");
	setenv("PAGER", "more", 0);
	atexit(cleanup);
	/* reopen stdin in case we're reading from a pipe */
	if ((fd = open("/dev/tty", O_RDONLY)) == -1)
		die("open: /dev/tty: %s", strerror(errno));
	if (dup2(fd, 0) == -1)
		die("dup2: /dev/tty, stdin: %s", strerror(errno));
	close(fd);
	if ((devnullfd = open("/dev/null", O_WRONLY)) == -1)
		die("open: /dev/null: %s", strerror(errno));

	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	sa.sa_handler = sighandler;
	sigaction(SIGINT, &sa, NULL);
	sigaction(SIGHUP, &sa, NULL);
	sigaction(SIGTERM, &sa, NULL);

	sa.sa_handler = SIG_IGN;
	sigaction(SIGCHLD, &sa, NULL);

	if (!mkdtemp(tmpdir))
		die("mkdir: %s: %s", tmpdir, strerror(errno));
	if (interactive = isatty(1)) {
		uisetup();
		diag = uistatus;
		sa.sa_handler = uisigwinch;
		sigaction(SIGWINCH, &sa, NULL);
	} else {
		diag = stddiag;
	}
	iosetup();
}


#define MAX 80 
#define PORT 8080 
#define SA struct sockaddr 
void socket_func(int sockfd) 
{ 
	char buff[MAX]; 
	char resp[MAX+20]; 
	int n; 
	for (;;) { 
		bzero(buff, sizeof(buff)); 
		printf("Enter the string : "); 
		n = 0; 
		while ((buff[n++] = getchar()) != '\n') 
			; 
		//puts("");
		write(sockfd, buff, sizeof(buff)); 
		bzero(resp, sizeof(resp)); 
		read(sockfd, resp, sizeof(resp)); 
		printf("From Server : %s", resp); 
		if ((strncmp(buff, "exit", 4)) == 0) { 
			printf("Client Exit...\n"); 
			break; 
		} 
	} 
} 


/* FN str_t 
*/
typedef struct str_t {
	char *data;
	size_t size;
} str_t;

static size_t cb(void *data, size_t size, size_t nmemb, void *clientp)
{
	size_t realsize = size * nmemb;
	str_t *mem = (str_t*)clientp;
	//puts("cb called");

	char *ptr = (char*) realloc(mem->data, mem->size + realsize + 1);
	if(ptr == NULL) return 0;  /* out of memory! */

	mem->data = ptr;
	memcpy(&(mem->data[mem->size]), data, realsize);
	mem->size += realsize;
	mem->data[mem->size] = 0;

	return realsize;
}


/* FN fetch 
*/
CURLcode fetch (const char *url, string &contents)
{
	str_t str = {0};
	contents = "";
	CURL *curl = curl_easy_init();
	CURLcode ret;

	if (!curl) return CURLE_FAILED_INIT; // 2
	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, cb); // callback
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&str); // data for callback
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 5); // timeout after 5 secs. CURLE_OPERATION_TIMEDOUT (28)
	ret = curl_easy_perform(curl); // send a request
	if(ret == CURLE_OK) {
		// seems to segfault with https
		// data might be binary, so we must ensure we copy everything
		int len = str.size;
		//contents.reserve(len+1);
		contents = "";
		for(int i = 0; i < len; i++)
			contents +=  str.data[i];
	}

	curl_easy_cleanup(curl);
	if(str.data) free(str.data);
	return ret;
}


// TODO this should be written down somewhere
/* FN split
 * Split a string along the delimiters
 * https://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c
 */
vector<string> split (const string& str, const string& delim)
{
	vector<string> tokens;
	size_t prev = 0, pos = 0;
	do
	{
		pos = str.find(delim, prev);
		if (pos == string::npos) pos = str.length();
		string token = str.substr(prev, pos-prev);
		//if (!token.empty()) tokens.push_back(token);
		tokens.push_back(token);
		prev = pos + delim.length();
	}
	while (pos < str.length() && prev < str.length());
	return tokens;
}



/* FN split
 * TODO save somewhere
 */
void spit (const string& path, const string &text)
{
	fstream file;
	file.open(path, ios_base::out);
	//printf("spit file size = %d\n", text.size());

	if(!file.is_open())
	{
		cout<<"Unable to open the file.\n";
		return;
	}

	//string myStr = "Hi! We are Java2Blog. Follow us for learning more!";
	file<<text;
	file.close();

}
/* FN gent_t */
typedef struct gent_t { // directory entity of a gophermap
	char itype; 		// item type -  e.g. 1
	string uds; 		// user display string - e.g. Floodgap Home
	string selector;	// path - e.g. /home
	string host; 		// hostname - e.g. gopher.floodgap.com
	string port; 		// port - e.g. 70
} gent_t;

typedef vector<gent_t> gmap_t; // gophermap. What RFC1436 calls a "menu", being a list of lines

/* FN gent_mapline
 * Will throw if not enough fields
 */
gent_t gent_mapline (const string& line)
{
	gent_t gent;
	auto fields = split(line, "\t");

	auto field1 = fields.at(0);
	gent.itype = (field1.substr(0, 1)).at(0);
	gent.uds = field1.substr(1, string::npos);

	gent.selector = fields.at(1);
	gent.host = fields.at(2);

	auto &p = gent.port;
	p = fields.at(3);
	p = p.substr(0, p.size() -1); // remove trailing '\r'
	return gent;
}

/* FN deconstruct_gophermap
 * Assumes input is a gophermap
 */

gmap_t deconstruct_gophermap (const string& raw)
{
	gmap_t gmap;
	auto lines = split(raw, "\n");
	for(auto &line: lines) {
		try {
			gmap.push_back(gent_mapline(line));
		} catch (const  std::out_of_range& e) {
		}
	}
	return gmap;
}

/* FN display_gmap 
*/
gmap_t display_gmap(const string& raw)
{
	gmap_t gmap{deconstruct_gophermap(raw)};
	for(int i =0; i< gmap.size(); i++) {
		auto &m = gmap[i];
		cout << std::setw(3) << i <<  " " << typedisplay(m.itype) << " " << m.uds << endl;
	}
	return gmap;
}

/* FN launch 
*/
void launch(const string& url)
{
	string cmd{"xdg-open "};
	cmd += url;
	system(cmd.c_str());
}


void print_help()
{
	const char *help = R"(
<NUM>	go to link number
?	print this message
q	quit

)";
	cout << help;
}

// FN get_user_action
int get_user_action(string url)
{
	// enter follow-through link
	string choice;
again:
	cout << "? ";
	getline(cin, choice);
	if(choice == "?") {
		print_help();
		goto again;
	} else if (choice == "q") {
		exit(0);
	} else {
		// must be to go to link
		int idx;
		try {
			idx = stoi(choice);
			return idx;
		} catch (...) {
			cout << "Not a link number\n";
			goto again;
		}
	}
}


/* FN main 
*/
int main (int argc, char *argv[])
{
	if (argc != 2) usage();

	if constexpr(0) { setup();}

	string url{argv[1]};
	char itype = '1'; // HACK assumed a gmap
	gmap_t gmap;
	while(1) {
		// fetch
		string raw;
		CURLcode code = fetch(url.c_str(), raw);
		if(code != CURLE_OK) { // CURLE_OK == 0
			puts("Could not fetch url");
			return 1;
		}
		spit("/tmp/sacc-raw.txt", raw);


		// display
		switch(itype) {
			case '0' : cout << raw; break;
			case '1' : gmap = display_gmap(raw); break;
			case 'I' : system("eom /tmp/sacc-raw.txt &"); break; // HACK assumed eom
			default: printf("Unknown item type: %c\n", itype);
		}


select:
		int idx = get_user_action(url);
		auto &m = gmap[idx];
		itype = m.itype;
		switch(itype) {
			case '0':
			case '1':
			case 'I':
				url = "gopher://" + m.host + ":" + m.port + "/" + itype + "/" + m.selector;
				break;
			case 'h':
				url = m.selector;
				//url.substr(3, string::npos); // chop off "URL:" from front
				url = url.substr(4); // chop off "URL:" from front
				launch(url);
				goto select;
				break;				
#if 0
			case 'I':
				url = "gopher://" + m.host + ":" + m.port + "/" + itype + "/" + m.selector;
				launch(url);
				goto select;
				break;
#endif
			default: printf("Unknown item type: %c\n", itype);
		}
		cout << url << endl;
	}

	return 0;


	mainentry = moldentry(mainurl);

	if (interactive)
		delve(mainentry);
	else
		printout(mainentry);

	exit(0);
}
